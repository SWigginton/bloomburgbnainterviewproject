﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BloomburgBNA.Models
{
    public class ExpenseItem
    {
        [Key]
        public long ID { get; set; }
        private DateTime date = DateTime.Now;
        [DisplayFormat(DataFormatString = "{MM/dd/yyyy}")]
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }
        [Required,MaxLength(50,ErrorMessage="50 characters is the max length")]
        public String Description { get; set; }
        [Required]
        public string Category { get; set; }
        public string SubCategory { get; set; }
        [DisplayFormat(DataFormatString="${0:F2}")]
        public float Amount { get; set; }
    }
}
﻿ko.bindingHandlers.date = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = valueAccessor(),
            allBindings = allBindingsAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);        
        if (valueUnwrapped == undefined || valueUnwrapped == null) {
            $(element).text("");
        }
        else {
            var unparsedDate = valueUnwrapped.toString().substring(0, 10).split("-");
            var mm = unparsedDate[1], dd = unparsedDate[2], yy = unparsedDate[0];
            $(element).val(mm+"/"+dd+"/"+yy);
        }
    }
}

ko.bindingHandlers.dateText = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = valueAccessor(),
            allBindings = allBindingsAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        if (valueUnwrapped == undefined || valueUnwrapped == null) {
            $(element).text("");
        }
        else {
            var unparsedDate = valueUnwrapped.toString().substring(0, 10).split("-");
            var mm = unparsedDate[1], dd = unparsedDate[2], yy = unparsedDate[0];
            $(element).text(mm + "/" + dd + "/" + yy);
        }
    }
}
function vm(data) {
    var self = this;
    ko.mapping.fromJSON(data, {}, self);
    self.SubmitNewItem = function () {
        if ($("#submitData").valid()) {
            var expenseitem = ko.toJS(self);
            delete expenseitem.SubmitNewItem;
            $.ajax({
                type: "POST",
                url: "api/ExpensePage",
                data: expenseitem,
                error: function (data, textStatus, jqXHR) {
                    alert(data + textStatus + jqXHR);
                },
                success: function () {
                    
                    self.items.push(expenseitem);
                    var test = self.items();
                }
            });
        } 
    };
    self.items = ko.observableArray([]);
}
jQuery.validator.addMethod("alphanumeric", function (value, element) {
    return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, and underscores only please");
$(function () {
    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    }, "Please specify a valid amount");

    $("#submissionDate").datepicker({
        minDate: new Date(2000, 0, 1),
        changeYear: true
    });

    //$("#totalAmount").blur(function () {
    //    var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
    //  val = this.value;

    //    if (!valid) {
    //        console.log("Invalid input!");
    //        this.value = val.substring(0, val.length - 1);
    //    } //http://jsfiddle.net/hibbard_eu/vY39r/
    //});
    $("#submitData").validate({
        rules: {
            submissionDate: "required",
            descriptionInp: {
                required:true,
                minLength:2,
                maxLength:50
            },
            category: {
                required: true,
                maxLength: 25,
                minLength: 2,
                alphanumeric:true
            },
            totalAmount:"required"
        }
    });
    $("#resultsTable").dataTable({ responsive: true });
    //$("#descriptionInp").rules("add", {
    //    maxLength: 50,
    //    messages: {
    //        required: "Please enter an item description",
    //        maxLength: jQuery.validator.format("{0} is the maximum amount of characters",50)
    //    }
    //});
    //$("#saveItem").click(function () {



    //});

});
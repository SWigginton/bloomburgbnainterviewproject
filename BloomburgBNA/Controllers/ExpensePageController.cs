﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BloomburgBNA.Models;

namespace BloomburgBNA.Controllers
{
    public class ExpensePageController : ApiController
    {
        private BloomburgBNAContext db = new BloomburgBNAContext();

        // GET api/ExpensePage
        public IEnumerable<ExpenseItem> GetExpenseItems()
        {
            return db.ExpenseItems.AsEnumerable();
        }

        // GET api/ExpensePage/5
        public ExpenseItem GetExpenseItem(long id)
        {
            ExpenseItem expenseitem = db.ExpenseItems.Find(id);
            if (expenseitem == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return expenseitem;
        }

        // PUT api/ExpensePage/5
        public HttpResponseMessage PutExpenseItem(long id, ExpenseItem expenseitem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != expenseitem.ID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(expenseitem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/ExpensePage
        public HttpResponseMessage PostExpenseItem(ExpenseItem expenseitem)
        {
            if (ModelState.IsValid)
            {
                db.ExpenseItems.Add(expenseitem);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, expenseitem);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = expenseitem.ID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/ExpensePage/5
        public HttpResponseMessage DeleteExpenseItem(long id)
        {
            ExpenseItem expenseitem = db.ExpenseItems.Find(id);
            if (expenseitem == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.ExpenseItems.Remove(expenseitem);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, expenseitem);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}